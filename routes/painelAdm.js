module.exports = function(app) {
	var painelAdm = app.controllers.painelAdm;
	var autenticar = require('../middleware/autenticar')
	app.route('/panel')
		.get(painelAdm.login)
		.post(painelAdm.autenticacao);

	app.route('/panel/home')
		.get(autenticar, painelAdm.index);

	app.route('/panel/logout')
		.get(autenticar, painelAdm.logout);

	app.route('/panel/email')
		.get(painelAdm.email)
		.post(painelAdm.enviar);
}