var nodemailer = require('nodemailer');

module.exports = function(app) {
		var Usuario = app.models.usuarios;
		var validacao = require('../validacoes/autenticacao');
		var HomeController = {
			index: function(req, res) {
				res.render('painelAdm/home/index');
			},
			login: function(req, res) {
				res.render('painelAdm/home/login');
			},
			autenticacao: function(req, res) {
				var usuario = new Usuario();
				var email = req.body.email;
				var password = req.body.password;

			if (validacao(req, res)) {
				Usuario.findOne({'email': email}, function(err, data) {
						if (err) {
							req.flash('erro', 'Erro ao entrar no sistema: ' + err);
							res.redirect('/panel');
						} else if (!data) {
							req.flash('erro', 'E-mail não encontrado');
							res.redirect('/panel');

						} else if (!usuario.validPassword(password, data.password)) {
							req.flash('erro', 'Senha não confere!');
							res.redirect('/panel');
						} else {
							req.session.usuario = data;
							res.redirect('/panel/home');
						}
					});

				}else{
					res.redirect('/panel');

				}
			},
			logout: function(req,res){
				req.session.destroy();
				res.redirect('/panel');
			},
			email: function(req,res){
				res.render('painelAdm/home/email')
			},
			enviar: function(req, res) {
				// EMAIL NÃO ESTA FUNCIONANDO

				var transport = nodemailer.createTransport({
					service: "gmail",
					host: "smtp.gmail.com",
					// host: 'smtp.mandrillapp.com',
					port: 465,
					auth: {
						user: 'williamstrayder@gmail.com',
						pass: 'Will-32060677'
					}
				});
				var mailOptions = {
					from: req.body.nome + " <" + req.body.email + ">",
					to: 'williamstrayder@gmail.com',
					subject: req.body.assunto,
					text: req.body.mensagem
				};
				transport.sendMail(mailOptions, function(err, response) {
					if (err) {
						req.flash('erro', 'Erro ao enviar e-mail' + err);
						res.redirect('/email');
					}
					req.flash('info', 'E-mail enviado som sucesso');
					res.render('painelAdm/home/email');
				});
			}
		}
			return HomeController;
		}